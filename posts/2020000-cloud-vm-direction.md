.. title: Cloud, VM, Direction
.. slug: cloud-vm-direction
.. date: 2020-09-29 14:17:56 UTC-04:00
.. tags: 
.. category: Project Direction
.. link: 
.. description: Future direction for the Ninja OS project
.. type: markdown


When we started with what would ultimately become Ninja OS in early 2010, Live
OSs where the latest hotness. Insperation came from previous generations of
Live CD distributions. Mostly early versions of TAILs that left users wanting
as well as AnonymOS, and other live Desktop OSes generally that got burned to
Optical disks such as CD and DVD. Initially, we did in fact provide an ISO9660
image for use with what is now legacy technology.

In the modern world, the Live OS has lost its lustre as well. Increasingly
people rely on virtual machines, and even distributions like Qubes which is
a management interface for virtual machines. The Live USB stick is not
completely worthless, and it will be kept.

As a concept, Ninja OS will move forward as a family of operating systems, and
tooling. They will all use the same Arch install base and similar and/or
complementory packages(such as client and server).

###Conceptualized Components:

* [ninjaos] - Arch Linux Repo
* Ninja LiveUSB
* Ninja VM Desktop
* Ninja remote VDI
* Ninja Cloud Server Template

