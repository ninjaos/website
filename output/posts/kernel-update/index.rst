.. title: Kernel Update - 5.10.x
.. slug: kernel-update
.. date: 2021-03-10 08:12:10 UTC-05:00
.. tags: maintenance
.. category: 
.. link: 
.. description: Updates to the -ninjaos kernel for Arch Linux
.. type: text

After a lot of thinking, We've come to the conclusion that having aufs as a
seperate package gained absolutely nothing, so we merged it into the main 
kernel package. Configuration of this module is now in our kernel config. We
feel that since aufs will ALWAYS be in use with Ninja OS, this will make testing
development, and maintenance easier as there is now one less package, especially
one that needs to be recompiled every kernel update anyway. 

Also in this kernel, we discovered there is a Kconfig option for XATTRs, or
extended attributes if underlying filesystems support them. both squashfs and
tmpfs do, and future versions of Ninja OS should be able to use POSIX
capablities for networking tools like ordinary OSes.

New version will hit [ninjaos-kernel] shortly. After upgrade you should pacman
-Rs aufs, as this is just the kernel object for
. PKGBUILD will be kept
around in git for a short time for posterity.

Version is also now centered around 5.10, and will continue to be adjusted
against newer longterm kernels. As always this the -ninja kernel is lts, with
-hardened, and aufs patches, and kernel interrupt timer set at 1000hz
